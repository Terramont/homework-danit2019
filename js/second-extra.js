'use strict';

function isNaturalNumber(string) {
    let temp = '';
    
    for (let i = 0; i < string.length; i++) {
        if (string[i] !== ' ') {
            temp += string[i];
        }
        if (string[i] === '.') {
            return false;
        }
    };
    
    if (!temp) return false;
    
    return !isNaN(Number(temp)) && (Number(temp) > 0);
};


let min = prompt('Enter lesser number, please');
let max = prompt('Enter bigger number, please');

while (!isNaturalNumber(min) || !isNaturalNumber(max) || (Number(min) >= Number(max))) {
    alert('Error, invalid values!');
    min = prompt('Enter lesser number, please');
    max = prompt('Enter bigger number, please');
};


for (let i = min; i <= max; i++) {
    for (let j = 2; j < max; j++) {
        if (j === i) continue;
        if (i % j === 0 ) i++;
    };
    console.log(i);
};