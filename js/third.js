'use strict';

/*
Функции существуют для повторения одного и того же кода в разных
частях программы. Аргумент в функцию передается, чтобы его обработать,
валидировать данные, изменить их, возможно создать копию аргумента и
работать с ней.
*/ 

const isNumber = string => {
    let temp = '';
    
    for (let i = 0; i < string.length; i++) {
        if (string[i] !== ' ') {
            temp += string[i];
        }
        if (string[i] === '.') {
            return false;
        }
    };
    
    if (!temp) return false;
    
    return !isNaN(Number(temp));
};

let num1 = prompt('Enter the first number');
let proc = prompt('Enter the procedure');
let num2 = prompt('Enter the second number');

while (!isNumber(num1) || !isNumber(num2)) {
    num1 = prompt('Enter the first number once more');
    num2 = prompt('Enter the second number once more');
};

while (proc !== '+' && proc !== '-' && proc !== '*' && proc !== '/') {
    proc = prompt('Enter the procedure once more');
}

const calculate = (val1, operator, val2) => {
    if (operator === '+') return val1 + val2;
    if (operator === '-') return val1 - val2;
    if (operator === '*') return val1 * val2;
    if (operator === '/') return val1 / val2;
}

console.log(calculate(+num1, proc, +num2));