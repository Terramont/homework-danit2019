/*
    Область видимости у let ограничена блоком('{}'),
    то есть в одном коде можно ее использовать несколько раз в разных блоках,
    переменную можно перезаписывать,
    var тоже ограничена блоком, но ее объявление всплывает на самый верх блока,
    ее также можно перезаписать,
    const имеет такую же обл.видимости как и let, но ее нельзя перезаписать
 */

let name = null
let age = null;

do {
name = prompt('Enter your name, please');
} while(!name || isNaN(Number(name));

do {
    age = prompt('Enter your age, please');
} while(!age || isNaN(Number(age)));

if (age < 18) {
    alert('You are not allowed to visit this website');
} else if (age <= 22) {
    let res = confirm('Are you sure you want to continue?');
    if (res) alert('Welcome' + name);
} else {
    alert('Welcome' + name);
};

