'use striict';

/*
* Циклы используются для повторения одного и того же куска кода
* такое количество раз, которое требуется
* */

function isNaturalNumber(string) {
    let temp = '';
    
    for (let i = 0; i < string.length; i++) {
        if (string[i] !== ' ') {
            temp += string[i];
        }
        if (string[i] === '.') {
            return false;
        }
    };
    
    if (!temp) return false;
    
    return !isNaN(Number(temp)) && (Number(temp) > 0);
};

let userInput = null;

do {
    userInput = prompt('Enter a number, please');
} while (!isNaturalNumber(userInput));

if (userInput < 0) alert('Sorry, no numbers')
 
for (let i = 0; i <= userInput; i +=5) {
    console.log(i);
};






